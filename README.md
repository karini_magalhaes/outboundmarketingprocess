# Test Automation Project - Outbound Marketing #

Test automation project for the selection process of Outbound Marketing Company.


### Architecture ###

Used tools:

* IDE: Eclipse Oxigen version 4.7.0
* Dependency Managment: Maven version 3.5.0
* Test Framework: Selenium WebDriver version 3.5.3
* Test Run: JUnit version 4.11
* Design Pattern: Page Objects

### Run Test Suite ###

Run the class AllRunTests in package br.com.karini.Settings.

Note: Execute with JUnit.


