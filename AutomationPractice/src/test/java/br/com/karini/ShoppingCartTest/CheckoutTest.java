package br.com.karini.ShoppingCartTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import br.com.karini.Pages.CheckoutPage;
import br.com.karini.Pages.HomePage;
import br.com.karini.Settings.Settings;

public class CheckoutTest {
	protected Settings settings;
	protected HomePage homePage;
	protected CheckoutPage checkoutPage;
	WebDriver driver;
	
	public CheckoutTest() {
		this.settings = new Settings();			
		this.driver = settings.getDriver();		
		this.homePage = new HomePage(driver);	
	}
	
	@Before
	public void setUp() {
		settings.navigateTo();
	}
	
	@Test
	public void validCheckout() {
		
		homePage.searchProducts("Faded Short");
		
		// move to page scroll for element view
		WebElement element = homePage.getDriver().findElement(By.xpath(".//*[@id='center_column']/ul/li/div/div[2]"));
		((JavascriptExecutor) homePage.getDriver()).executeScript("arguments[0].scrollIntoView();", element); 
		
		homePage.addProduct(".//*[@id='center_column']/ul/li/div/div[2]", ".//*[@id='center_column']/ul/li/div/div[2]/div[2]/a[1]/span");
		
		homePage.proceedToCheckout();
		
		checkoutPage = new CheckoutPage(driver);
		
		checkoutPage.checkoutSign();
		
		String message = checkoutPage.getDriver().findElement(By.xpath(".//*[@id='center_column']/div/p/strong")).getText();
		Assert.assertEquals("Your order on My Store is complete.", message);
		
	}
	
	@After
	public void closeDriver() {
		settings.closeNavigator();
	}
}
