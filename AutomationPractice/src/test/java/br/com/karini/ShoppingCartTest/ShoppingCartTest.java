package br.com.karini.ShoppingCartTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import br.com.karini.Pages.HomePage;
import br.com.karini.Settings.Settings;


public class ShoppingCartTest {
	
	protected Settings settings;
	protected HomePage homePage;
	WebDriver driver;
	
	public ShoppingCartTest() {
		this.settings = new Settings();			
		this.driver = settings.getDriver();		
		this.homePage = new HomePage(driver);	
	}
	
	@Before
	public void setUp() {
		settings.navigateTo();
	}
	
	@Test
	public void addProductCart() {
		
		homePage.searchProducts("Faded Short");
		
		// move to page scroll for element view
		WebElement element = homePage.getDriver().findElement(By.xpath(".//*[@id='center_column']/ul/li/div/div[2]"));
		((JavascriptExecutor) homePage.getDriver()).executeScript("arguments[0].scrollIntoView();", element); 
		
		homePage.addProduct(".//*[@id='center_column']/ul/li/div/div[2]", ".//*[@id='center_column']/ul/li/div/div[2]/div[2]/a[1]/span");
		
		homePage.continueShopping();
		
		String amountProduct = homePage.viewAmountProducts();
		Assert.assertEquals("1", amountProduct);
	}
	
	@Test
	public void removeProductCart() {
		
		// Add product to Shopping cart
		homePage.searchProducts("Faded Short");
		
		// move to page scroll for element view
		WebElement element = homePage.getDriver().findElement(By.xpath(".//*[@id='center_column']/ul/li/div/div[2]"));
		((JavascriptExecutor) homePage.getDriver()).executeScript("arguments[0].scrollIntoView();", element); 
		
		homePage.addProduct(".//*[@id='center_column']/ul/li/div/div[2]", ".//*[@id='center_column']/ul/li/div/div[2]/div[2]/a[1]/span");
		homePage.continueShopping();
		
		// Remove product to Shopping cart
		homePage.removeProduct();
		String amountProduct = homePage.viewAmountProducts();
		Assert.assertEquals("(empty)", amountProduct);
	}
	
	@After
	public void closeDriver() {
		settings.closeNavigator();
	}
}
