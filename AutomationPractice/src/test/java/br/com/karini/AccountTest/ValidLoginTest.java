package br.com.karini.AccountTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.com.karini.Pages.HomePage;
import br.com.karini.Pages.LoginPage;
import br.com.karini.Settings.Settings;

public class ValidLoginTest {
	protected Settings settings;
	protected HomePage homePage;
	protected LoginPage loginPage;
	WebDriver driver;
	
	public ValidLoginTest() {
		this.settings = new Settings();			
		this.driver = settings.getDriver();		
		this.homePage = new HomePage(driver);	
	}
	
	@Before
	public void setUp() {
		settings.navigateTo();
	}
	
	@Test
	public void validLoginTest() {
		homePage.signIn();
		loginPage = new LoginPage(driver);
		loginPage.login("application@test.com", "123456");
		String userLogged = loginPage.getDriver().findElement(By.xpath(".//*[@id='header']/div[2]/div/div/nav/div[1]/a/span")).getText();
		Assert.assertEquals(userLogged, "Application Test");
	}
	
	@After
	public void closeDriver() {
		settings.closeNavigator();
	}
}
