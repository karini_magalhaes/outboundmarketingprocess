package br.com.karini.AccountTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.com.karini.Pages.HomePage;
import br.com.karini.Pages.LoginPage;
import br.com.karini.Settings.Settings;

public class InvalidEmailLoginTest {
	protected Settings settings;
	protected HomePage homePage;
	protected LoginPage loginPage;
	WebDriver driver;
	
	public InvalidEmailLoginTest() {
		this.settings = new Settings();			
		this.driver = settings.getDriver();		
		this.homePage = new HomePage(driver);	
	}
	
	@Before
	public void setUp() {
		settings.navigateTo();
	}
	
	@Test
	public void invalidEmailTest() {
		homePage.signIn();
		loginPage = new LoginPage(driver);
		loginPage.login("123", "123456");
		String message = loginPage.getDriver().findElement(By.xpath(".//*[@id='center_column']/div[1]/ol/li")).getText();
		Assert.assertEquals(message, "Invalid email address.");
	}
	
	@Test
	public void invalidPasswordTest() {
		homePage.signIn();
		loginPage = new LoginPage(driver);
		loginPage.login("application@test.com", "abcdef");
		String message = loginPage.getDriver().findElement(By.xpath(".//*[@id='center_column']/div[1]/ol/li")).getText();
		Assert.assertEquals(message, "Invalid password.");
	}
	
	@Test
	public void emptyEmailTest() {
		homePage.signIn();
		loginPage = new LoginPage(driver);
		loginPage.login("", "");
		String message = loginPage.getDriver().findElement(By.xpath(".//*[@id='center_column']/div[1]/ol/li")).getText();
		Assert.assertEquals(message, "An email address required.");
	}
	
	@Test
	public void emptyPasswordTest() {
		homePage.signIn();
		loginPage = new LoginPage(driver);
		loginPage.login("123", "");
		String message = loginPage.getDriver().findElement(By.xpath(".//*[@id='center_column']/div[1]/ol/li")).getText();
		Assert.assertEquals(message, "Password is required.");
	}
	
	@After
	public void closeDriver() {
		settings.closeNavigator();
	}
}
