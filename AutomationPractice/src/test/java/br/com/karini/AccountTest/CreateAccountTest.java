package br.com.karini.AccountTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import br.com.karini.Pages.CreateAccountPage;
import br.com.karini.Pages.HomePage;
import br.com.karini.Settings.Settings;

public class CreateAccountTest {
	protected Settings settings;
	protected HomePage homePage;
	protected CreateAccountPage createAccountPage;
	WebDriver driver;
	
	public CreateAccountTest() {
		this.settings = new Settings();			
		this.driver = settings.getDriver();		
		this.homePage = new HomePage(driver);	
	}
	
	@Before
	public void setUp() {
		settings.navigateTo();
	}
	
	@Test
	public void emailAccountTest() {
		homePage.signIn();
		createAccountPage = new CreateAccountPage(driver);
		createAccountPage.createEmail("automation7@test.com");
		String email = ((JavascriptExecutor) createAccountPage.getDriver()).executeScript("return $('#email').val()").toString();
		Assert.assertEquals(email, "automation6@test.com");
	}
	
	@Test
	public void createAccountTest() {
		homePage.signIn();
		createAccountPage = new CreateAccountPage(driver);
		createAccountPage.createEmail("automation7@test.com");
		createAccountPage.createAccount("Automation", "Test", "123456", "02", "January", "2000", "Automation Address", 
				"Test Address", "Ountbound Marketing", "Avenue One", "Street Two", "New Jersey", "New Hersey", "111111", 
				"United State", "Welcome!", "999999999", "8888888", "My Address");
		
		String userLogged = createAccountPage.getDriver().findElement(By.xpath(".//*[@id='header']/div[2]/div/div/nav/div[1]/a/span")).getText();
		Assert.assertEquals(userLogged, "Automation Test");
	}
	
	@After
	public void closeDriver() {
		settings.closeNavigator();
	}
}
