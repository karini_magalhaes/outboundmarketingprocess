package br.com.karini.AccountTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.com.karini.Pages.CreateAccountPage;
import br.com.karini.Pages.HomePage;
import br.com.karini.Settings.Settings;

public class InvalidAccountTest {
	protected Settings settings;
	protected HomePage homePage;
	protected CreateAccountPage createAccountPage;
	WebDriver driver;
	
	public InvalidAccountTest() {
		this.settings = new Settings();			
		this.driver = settings.getDriver();		
		this.homePage = new HomePage(driver);	
	}
	
	@Before
	public void setUp() {
		settings.navigateTo();
	}
	
	@Test
	public void emailUsingTest() {
		homePage.signIn();
		createAccountPage = new CreateAccountPage(driver);
		createAccountPage.createEmail("automation7@test.com");
		String message = createAccountPage.getDriver().findElement(By.xpath(".//*[@id='create_account_error']/ol/li")).getText();
		Assert.assertEquals(message, "An account using this email address has already been registered. Please enter a valid "
				+ "password or request a new one.");
	}
	
	@Test
	public void invalidEmailTest() {
		homePage.signIn();
		createAccountPage = new CreateAccountPage(driver);
		createAccountPage.createEmail("automation7@te");
		String message = createAccountPage.getDriver().findElement(By.xpath(".//*[@id='create_account_error']/ol/li")).getText();
		Assert.assertEquals(message, "Invalid email address.");
	}
	
	
	@After
	public void closeDriver() {
		settings.closeNavigator();
	}
}
