package br.com.karini.Settings;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.karini.AccountTest.CreateAccountTest;
import br.com.karini.AccountTest.InvalidAccountTest;
import br.com.karini.AccountTest.InvalidEmailLoginTest;
import br.com.karini.AccountTest.ValidLoginTest;
import br.com.karini.ShoppingCartTest.CheckoutTest;
import br.com.karini.ShoppingCartTest.ShoppingCartTest;

@RunWith(Suite.class)
@SuiteClasses({
	ValidLoginTest.class,
	InvalidEmailLoginTest.class,
	InvalidAccountTest.class,
	CreateAccountTest.class,
	CheckoutTest.class,
	ShoppingCartTest.class	
})

public class RunAllTests {

}
