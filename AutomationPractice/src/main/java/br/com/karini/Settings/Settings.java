package br.com.karini.Settings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Settings {
	
	WebDriver driver;
	
	public Settings(WebDriver driver){
		this.driver = driver;
	}
	
	public Settings(){
		System.setProperty("webdriver.gecko.driver", "c:\\geckodriver.exe");
		this.driver = new FirefoxDriver();
	}
	
	public void navigateTo(){
		driver.navigate().to("http://automationpractice.com/index.php");
	}
	
	public WebDriver getDriver(){
		return driver;
	}
	
	public void closeNavigator(){
		getDriver().quit();
	}
	
	public void waitElements(String xpath){
		WebDriverWait wait = new WebDriverWait(getDriver(), 300);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	public void waitElementsId(String id){
		WebDriverWait wait = new WebDriverWait(getDriver(), 300);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
	}
	
	public void selectCombobox(String id, String value) {
		getDriver().findElement(By.id(id)).click();
		WebElement comboboxValues = getDriver().findElement(By.id(id));
		Select comboboxValue = new Select(comboboxValues);
		comboboxValue.selectByValue(value);
	}

	
}