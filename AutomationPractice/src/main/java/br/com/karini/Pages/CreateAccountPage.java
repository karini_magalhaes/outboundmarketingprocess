package br.com.karini.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateAccountPage extends HomePage{

	public CreateAccountPage(WebDriver driver) {
		super(driver);
	}
	
	public void createEmail (String email) {
		getDriver().findElement(By.id("email_create")).sendKeys(email);
		getDriver().findElement(By.id("SubmitCreate")).click();
	}
	
	public void createAccount(String firstName, String lastName, String password, String day, String month, String year,
			String firstNameAddress, String lastNameAddress, String company, String address, String address2, String city, 
			String state, String postalCode, String country, String additionalInformation, String homePhone, String mobilePhone, String reference) {
		
		// Personal Information
		
		waitElementsId("customer_firstname");
		getDriver().findElement(By.id("customer_firstname")).sendKeys(firstName);
		getDriver().findElement(By.id("customer_lastname")).sendKeys(lastName);
		getDriver().findElement(By.id("passwd")).sendKeys(password);
		getDriver().findElement(By.id("passwd")).sendKeys(password);

		waitElementsId("days");
		getDriver().findElement(By.id("days")).click();
		selectCombobox("days", day);
		
		waitElementsId("months");
		getDriver().findElement(By.id("months")).click();
		selectCombobox("months", month);
		
		waitElementsId("years");
		getDriver().findElement(By.id("years")).click();
		selectCombobox("years", year);
		
		// Address Information
		getDriver().findElement(By.id("firstname")).sendKeys(firstNameAddress);
		getDriver().findElement(By.id("lastname")).sendKeys(lastNameAddress);
		getDriver().findElement(By.id("company")).sendKeys(company);
		getDriver().findElement(By.id("address1")).sendKeys(address);
		getDriver().findElement(By.id("address2")).sendKeys(address2);
		getDriver().findElement(By.id("city")).sendKeys(city);
		
		waitElementsId("id_state");
		getDriver().findElement(By.id("id_state")).click();
		selectCombobox("id_state", state);
		
		getDriver().findElement(By.id("postcode")).sendKeys(postalCode);
		
		waitElementsId("id_country");
		getDriver().findElement(By.id("id_country")).click();
		selectCombobox("id_country", country);
		
		getDriver().findElement(By.id("other")).sendKeys(additionalInformation);
		getDriver().findElement(By.id("phone")).sendKeys(homePhone);
		getDriver().findElement(By.id("phone_mobile")).sendKeys(mobilePhone);
		getDriver().findElement(By.id("alias")).sendKeys(reference);
		
		// Submit
		getDriver().findElement(By.id("submitAccount")).click();

	}
	
}
