package br.com.karini.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.com.karini.Settings.Settings;

public class LoginPage extends Settings {

	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public void login(String email, String password) {
		getDriver().findElement(By.id("email")).sendKeys(email);
		getDriver().findElement(By.id("passwd")).sendKeys(password);
		getDriver().findElement(By.id("SubmitLogin")).click();
	}
}
