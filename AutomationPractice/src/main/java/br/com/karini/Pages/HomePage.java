package br.com.karini.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import br.com.karini.Settings.Settings;

public class HomePage extends Settings {
		
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public void signIn() {
		getDriver().findElement(By.className("login")).click();
	}
	
	public void searchProducts(String product) {
		getDriver().findElement(By.id("search_query_top")).sendKeys(product);
		getDriver().findElement(By.name("submit_search")).click();
	}
	
	public String viewAmountProducts() {
		return getDriver().findElement(By.xpath("//*[@id='header']/div[3]/div/div/div[3]/div/a/span[1]")).toString();
	}
	
	public void addProduct(String xpathProduct, String xpathAddCart) {
		Actions moveMouse = new Actions(getDriver());
		waitElements(xpathProduct);
		moveMouse.moveToElement(getDriver().findElement(By.xpath(xpathProduct)));
		moveMouse.moveToElement(getDriver().findElement(By.xpath(xpathAddCart)));
		moveMouse.click().perform();	
	}
	
	public void removeProduct() {
		Actions moveMouse = new Actions(getDriver());
		waitElements(".//*[@id='header']/div[3]/div/div/div[3]/div/a");
		moveMouse.moveToElement(getDriver().findElement(By.xpath(".//*[@id='header']/div[3]/div/div/div[3]/div/a")));
		
		getDriver().findElement(By.xpath(".//*[@id='header']/div[3]/div/div/div[3]/div/div/div/div/dl/dt/span/a")).click();
	}
	
	public void proceedToCheckout() {
		getDriver().findElement(By.xpath(".//*[@id='layer_cart']/div[1]/div[2]/div[4]/a/span")).click();
	}
	
	public void continueShopping() {
		getDriver().findElement(By.xpath(".//*[@id='layer_cart']/div[1]/div[2]/div[4]/span/span")).click();
	}

}
