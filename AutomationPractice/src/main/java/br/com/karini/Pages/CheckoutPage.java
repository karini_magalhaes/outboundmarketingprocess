package br.com.karini.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.com.karini.Settings.Settings;

public class CheckoutPage extends Settings {

	LoginPage loginPage;
	
	public CheckoutPage(WebDriver driver) {
		super(driver);
	}
	
	public void checkoutSign() {
				
		// Checkout stage 1 - Summary
		getDriver().findElement(By.xpath(".//*[@id='center_column']/p[2]/a[1]/span")).click();
		
		// Stage 2 - Login
		loginPage.login("application@test.com", "123456");
		
		//Checkout 3 - Address
		getDriver().findElement(By.name("processAddress")).click();
		
		// Checkout 4 - Shipping
		//Acceptance Terms of service
		getDriver().findElement(By.id("cgv")).click();
		getDriver().findElement(By.xpath(".//*[@id='form']/p/button")).click();
		
		getDriver().findElement(By.name("processCarrier")).click();
		
		// Stage 5 - Payment (by Bank wire)
		getDriver().findElement(By.xpath(".//*[@id='HOOK_PAYMENT']/div[1]/div/p/a")).click();
		
		// Confirm order
		getDriver().findElement(By.xpath(".//*[@id='cart_navigation']/button")).click();
	}
	
	
}
